package ru.t1.godyna.tm.dto.response.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.godyna.tm.dto.model.TaskDTO;

@NoArgsConstructor
public final class TaskShowByIdResponse extends AbstractTaskResponse {

    public TaskShowByIdResponse(@Nullable final TaskDTO task) {
        super(task);
    }

}
