package ru.t1.godyna.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.godyna.tm.api.model.ICommand;
import ru.t1.godyna.tm.command.AbstractCommand;

import java.util.Collection;

@Component
public final class ApplicationListCommand extends AbstractSystemCommand {

    @NotNull
    private final String NAME = "commands";

    @NotNull
    private final String ARGUMENT = "-cmd";

    @NotNull
    private final String DESCRIPTION = "Show command list.";

    @Override
    public void execute() {
        System.out.println("[COMMANDS]");
        @NotNull final Collection<AbstractCommand> commands = commandService.getCommands();
        for (@Nullable final ICommand command: commands) {
            if (command == null) continue;
            @Nullable final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name.toString());
        }
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
