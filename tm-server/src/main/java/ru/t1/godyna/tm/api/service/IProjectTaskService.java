package ru.t1.godyna.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.godyna.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1.godyna.tm.api.repository.dto.ITaskDtoRepository;

import javax.persistence.EntityManager;

public interface IProjectTaskService {

    @NotNull
    ITaskDtoRepository getTaskRepository(@NotNull EntityManager entityManager);

    @NotNull
    IProjectDtoRepository getProjectRepository(@NotNull EntityManager entityManager);

    void bindTaskToProject(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskId);

    void removeProjectById(@Nullable final String userId, @Nullable final String projectId);

    void unbindTaskFromProject(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskId);

}
