package ru.t1.godyna.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.t1.godyna.tm.api.service.IPropertyService;

import java.util.Properties;

@Service
public class PropertyService implements IPropertyService {

    @NotNull
    public static final String FILE_NAME = "application.properties";

    @NotNull
    private static final String APPLICATION_VERSION_KEY = "buildNumber";

    @NotNull
    private static final String AUTHOR_EMAIL_KEY = "email";

    @NotNull
    private static final String AUTHOR_NAME_KEY = "developer";

    @NotNull
    private static final String PASSWORD_ITERATION_DEFAULT = "25631";

    @NotNull
    private static final String PASSWORD_ITERATION_KEY = "password.iteration";

    @NotNull
    private static final String PASSWORD_SECRET_DEFAULT = "3434562535";

    @NotNull
    private static final String PASSWORD_SECRET_KEY = "password.secret";

    @NotNull
    private static final String SERVER_HOST_KEY = "server.host";

    @NotNull
    private static final String SESSION_KEY = "session.key";

    @NotNull
    private static final String SESSION_KEY_DEFAULT = "gvrgg234rg";

    @NotNull
    private static final String SERVER_HOST_DEFAULT = "localhost";

    @NotNull
    private static final String SESSION_TYMEOUT_KEY = "session.timeout";

    @NotNull
    private static final String SESSION_TYMEOUT_DEFAULT = "100000";

    @NotNull
    private static final String SERVER_PORT_KEY = "server.port";

    @NotNull
    private static final String SERVER_PORT_DEFAULT = "6060";

    @NotNull
    private static final String DATABASE_DIALECT = "database.dialect";

    @NotNull
    private static final String EMPTY_VALUE = "---";

    @NotNull
    private static final String DATABASE_URL = "database.url";

    @NotNull
    private static final String DATABASE_USERNAME = "database.username";

    @NotNull
    private static final String DATABASE_USERNAME_DEFAULT = "postgres";

    @NotNull
    private static final String DATABASE_PASSWORD = "database.password";

    @NotNull
    private static final String DATABASE_PASSWORD_DEFAULT = "123";

    @NotNull
    private static final String DATABASE_DRIVER = "database.driver";

    @NotNull
    private static final String DATABASE_DRIVER_DEFAULT = "org.postgresql.Driver";

    @NotNull
    private static final String DATABASE_DHM2DDLAUTO = "database.hbm2ddl_auto";

    @NotNull
    private static final String DATABASE_SHOWSQL = "database.show_sql";

    @NotNull
    private static final String DATABASE_SECOND_LEVEL_CACHE = "database.second_lvl_cache";

    @NotNull
    private static final String DATABASE_FACTORY_CLASS = "database.factory_class";

    @NotNull
    private static final String DATABASE_USE_QUERY_CACHE = "database.use_query_cache";

    @NotNull
    private static final String DATABASE_USE_MIN_PUTS = "database.use_min_puts";

    @NotNull
    private static final String DATABASE_REGION_PREFIX = "database.region_prefix";

    @NotNull
    private static final String DATABASE_CONFIG_FILE_PATH = "database.config_file_path";

    @NotNull
    private static final String DATABASE_FORMAT_SQL = "database.format_sql";

    @NotNull
    private static final String DATABASE_FORMAT_SQL_DEFAULT = "false";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        properties.load(ClassLoader.getSystemResourceAsStream(FILE_NAME));
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        return Manifests.read(APPLICATION_VERSION_KEY);
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        return Manifests.read(AUTHOR_EMAIL_KEY);
    }

    @NotNull
    @Override
    public String getAuthorName() {
        return Manifests.read(AUTHOR_NAME_KEY);
    }

    @Override
    @NotNull
    public String getServerHost() {
        return getStringValue(SERVER_HOST_KEY, SERVER_HOST_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getServerPort() {
        return getIntegerValue(SERVER_PORT_KEY, SERVER_PORT_DEFAULT);
    }

    @NotNull
    @Override
    public String getSessionKey() {
        return getStringValue(SESSION_KEY, SESSION_KEY_DEFAULT);
    }

    @NotNull
    private String getEnvKey(@NotNull final String key) {
        return key.replace(".", "_").toUpperCase();
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        return getIntegerValue(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT);
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        return getStringValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getSessionTimeout() {
        return getIntegerValue(SESSION_TYMEOUT_KEY, SESSION_TYMEOUT_DEFAULT);
    }

    @NotNull
    private Integer getIntegerValue(@NotNull final String key, @NotNull final String defaultValue) {
        return Integer.parseInt(getStringValue(key, defaultValue));
    }

    @NotNull
    private String getStringValue(@NotNull final String key, @NotNull final String defaultValue) {
        if (System.getProperties().contains(key)) return System.getProperties().getProperty(key);
        @NotNull final String envKey = getEnvKey(key);
        if (System.getenv().containsKey(envKey)) return System.getenv(envKey);
        return properties.getProperty(key, defaultValue);
    }

    @NotNull
    private String getStringValue(@NotNull final String key) {
        return getStringValue(key, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getDatabaseURL() {
        return getStringValue(DATABASE_URL);
    }

    @NotNull
    @Override
    public String getDatabaseUsername() {
        return getStringValue(DATABASE_USERNAME, DATABASE_USERNAME_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabasePassword() {
        return getStringValue(DATABASE_PASSWORD, DATABASE_PASSWORD_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabaseDriver() {
        return getStringValue(DATABASE_DRIVER, DATABASE_DRIVER_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabaseDialect() {
        return getStringValue(DATABASE_DIALECT);
    }

    @NotNull
    @Override
    public String getDatabaseHbm2DdlAuto() {
        return getStringValue(DATABASE_DHM2DDLAUTO);
    }

    @NotNull
    @Override
    public String getDatabaseShowSql() {
        return getStringValue(DATABASE_SHOWSQL);
    }

    @NotNull
    @Override
    public String getDatabaseSecondLevelCache() {
        return getStringValue(DATABASE_SECOND_LEVEL_CACHE);
    }

    @NotNull
    @Override
    public String getDatabaseFactoryClass() {
        return getStringValue(DATABASE_FACTORY_CLASS);
    }

    @NotNull
    @Override
    public String getDatabaseUseQueryCache() {
        return getStringValue(DATABASE_USE_QUERY_CACHE);
    }

    @NotNull
    @Override
    public String getDatabaseUseMinPuts() {
        return getStringValue(DATABASE_USE_MIN_PUTS);
    }

    @NotNull
    @Override
    public String getDatabaseRegionPrefix() {
        return getStringValue(DATABASE_REGION_PREFIX);
    }

    @NotNull
    @Override
    public String getDatabaseConfigFilePath() {
        return getStringValue(DATABASE_CONFIG_FILE_PATH);
    }

    @Override
    public @NotNull String getDatabaseFormatSql() {
        return getStringValue(DATABASE_FORMAT_SQL, DATABASE_FORMAT_SQL_DEFAULT);
    }

}
